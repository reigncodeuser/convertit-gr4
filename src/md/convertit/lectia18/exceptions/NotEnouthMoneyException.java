package md.convertit.lectia18.exceptions;

/**
 * Created by Utilizator on 08.06.2017.
 */
public class NotEnouthMoneyException extends Exception {

    public NotEnouthMoneyException(String message) {
        super(message);
    }
}
