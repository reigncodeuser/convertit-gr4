package md.convertit.lectia18.exceptions;

/**
 * Created by Utilizator on 08.06.2017.
 */
public class NoSuchProductExeption extends Exception {
    private String missingProduct;

    public NoSuchProductExeption(String missingProduct) {
        //setez mesaj de exceptie in clasa Exception
        super(String.format("Produsul %s nu este disponibil", missingProduct));
        this.missingProduct = missingProduct;

    }
}
