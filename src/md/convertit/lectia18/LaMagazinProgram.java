package md.convertit.lectia18;

import md.convertit.lectia18.exceptions.NoSuchProductExeption;
import md.convertit.lectia18.exceptions.NotEnouthMoneyException;

import javax.swing.*;

/**
 * Created by Utilizator on 08.06.2017.
 */
public class LaMagazinProgram {

    public static void main(String[] args) {
        //creez un obiect de tip Magazin
        Magazin magazin = new Magazin();
        magazin.displayProduse();
        //salutam client, si il rugam sa scrie produsele dorite
        String userInput = JOptionPane.showInputDialog("Buna ziua, scriti lista de produse" +
                "dupa format: produs1,produs2,produsN");
        String[] produse = userInput.split(",");
        try {
            int pretTotal = magazin.calculeazaTotalPret(produse, 30);
            JOptionPane.
                    showMessageDialog(null,
                            String.format("Achitati suma %d lei", pretTotal));
        } catch (NoSuchProductExeption e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } catch (NotEnouthMoneyException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

    }
}
