package md.convertit.lectia18;

import md.convertit.lectia18.exceptions.NoSuchProductExeption;
import md.convertit.lectia18.exceptions.NotEnouthMoneyException;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Utilizator on 08.06.2017.
 */
public class Magazin {
    private Map<String, Integer> produse;

    public Magazin() {
        initProduse();
    }

    /**
     * @param produse         lista de produse dorite
     * @param sumaDisponibila suma de
     * @return pretul total al produselor
     */
    public int calculeazaTotalPret(String[] produse, int sumaDisponibila)
            throws NoSuchProductExeption, NotEnouthMoneyException {
        int pret = 0;
        for (int i = 0; i < produse.length; i++) {
            String product = produse[i];
            Integer pretProudus = this.produse.get(product);
            if (pretProudus != null) {
                //daca am gasit produs, adum pretul la cel initial
//                pret = pret + pretProudus;
                pret += pretProudus;
            } else {
                //arunc eroare de tip NoSuchProduct
                throw new NoSuchProductExeption(product);
                //in caz de exceptie, se opreste executia blocului
            }

            //verificam daca summa produselor este mai mica ca cea disponibila
            if (pret > sumaDisponibila) {
                //aruncam eroare fonduri insuficiente
                throw new NotEnouthMoneyException
                        (String.format("Suma %d este insuficienta, ar trebui %d",
                                sumaDisponibila, pret));
                //se opreste executia
            }
        }
        return pret;//returnam pret calculat
    }

    private void initProduse() {
        produse = new HashMap<>();
        produse.put("unt", 20);
        produse.put("paine", 5);
        produse.put("apa", 7);
        produse.put("zahar", 17);
    }

    public void displayProduse() {
        //afisam produsele la raft
        Set<String> keys = produse.keySet();
        for (String key : keys) {
            System.out.printf("* %s  - %d\n", key, produse.get(key));
        }
    }
}
