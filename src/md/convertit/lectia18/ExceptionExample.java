package md.convertit.lectia18;

import javax.swing.*;
import java.io.FileNotFoundException;

/**
 * Created by Utilizator on 08.06.2017.
 */
public class ExceptionExample {

    public static void main(String[] args) throws FileNotFoundException {
        String[] myArray = new String[3];
        //adaugam elemente in array

        myArray[0] = "mama";
        myArray[1] = "tata";
        myArray[2] = "sora";

        try {//deschidem blocul try
            myArray[3] = "frate";
        } catch (ArrayIndexOutOfBoundsException e) {//prindem eroarea
            JOptionPane.showMessageDialog(null, "Ai depasit index-urile permise");
        } finally {//se executa si la success si la eroare
            System.out.println("trecem prin finaly");
        }

        System.out.println("Elementele tabloului sunt:");
        for (int i = 0; i < myArray.length; i++) {
            System.out.printf("Pe index %d avem valoare %s\n", i, myArray[i]);
        }
    }
}
