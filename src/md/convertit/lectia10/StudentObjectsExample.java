package md.convertit.lectia10;

/**
 * Created by Utilizator on 08.05.2017.
 */
public class StudentObjectsExample {
    public static void main(String[] args) {
        //cream 2 obiect de tip Student
        //primul=> s1 => il declaram si initializam in randuri diferite
        // int i; => declarare
        // i = 10; => initializare
        //declara o variabila de tip Student, numele variabilei : s1
        Student s1 = null;
        System.out.printf("pana la initiliza s1 are valoarea: %s\n", s1);
        s1 = new Student();

        System.out.printf("s1 nume: %s\n", s1.nume);//null
        System.out.printf("s1 nota: %d\n", s1.nota); // 0

//cream un obiect de tip Student, s2 , declaram si initializam in acelasi rand
        Student s2 = new Student();
        System.out.printf("s2 nume: %s\n", s2.nume);//null
        System.out.printf("s2 nota: %d\n", s2.nota); // 0

        //atribuim obiectului s1 , pentru cimpul nume, valoare: Jena
        s1.nume = "Jenna";
        //atribuim obiectului s1 , pentru cimpul nota, valoare: 10
        s1.nota = 10;

        s2.nume = "John";
        s2.nota = 8;

        //afisam mesaj in forma:
        // Ion are nota 10
        System.out.printf("Studentul %s are nota %d\n", s1.nume, s1.nota);
        System.out.printf("Studentul %s are nota %d\n", s2.nume, s2.nota);

        int notaStudent = s1.nota;
        System.out.printf("valoare lui notaStudent este %d\n", notaStudent);

        //verificam daca nota lui Jena e mai mare ca nota lui John
        // Jena ?=> s1
        // John ?=> s2
        //comparam daca nota lui Jena e mai mare ca a lui John
        boolean isBigger = (s1.nota > s2.nota);
        System.out.printf("%s are nota mai mare ca %s : %s", s1.nume, s2.nume, isBigger);
    }
}
