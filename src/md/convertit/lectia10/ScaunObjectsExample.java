package md.convertit.lectia10;

/**
 * Created by Utilizator on 08.05.2017.
 */
public class ScaunObjectsExample {
    public static void main(String[] args) {
        //facem un obiect de tip Scaun!

        //<NumeClasa> numeObiect  = new <NumeClasa>();
        Scaun sc1 = new Scaun();
        //Scaun -> este numele clasei
        //sc1 -> este numele obiectului de tip Scaun
        //setam nr de scaune pentru obiectul sc1
        // <numeObiect>.<numeAtribut>
        // nota = 7; -> exemple tip primitiv
        sc1.numarPicioare = 4; // exemplu variabila din obiect
        //afisam la consola numar de picioare pentur obiect sc1
        System.out.println(sc1.numarPicioare);
        //atribuim culoare pentru obiectul sc1
        sc1.culoare = "cafeniu";
        System.out.printf("sc1 are culoarea %s", sc1.culoare);
    }
}
