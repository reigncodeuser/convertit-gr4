package md.convertit.lectia10;

/**
 * Created by Utilizator on 08.05.2017.
 */
public class StringExercice {
    //comunism, socialism -> ******
    //eu sunt comunist din socialism
    //1-> eu sunt ***** din socialisim
    //2-> sunt ***** din *****

    public static void main(String[] args) {
        // lista de cuvinte necenzuarat
        String[] restrictedWords = {"comunism", "Socialism", "Dodon", "bou"};

        String text = "Eu is dodon, am fost in comunism dar am ajuns la socialism";
        //text to lowerCase
        text = text.toLowerCase();
        //pentru fiecare cuvint interzis facem replace
        for (int i = 0; i < restrictedWords.length; i++) {
            String word = restrictedWords[i];
            //toLowerCase
            word = word.toLowerCase();
            //daca textul contine cuvintul interzis, atunci fac replace
            if (text.contains(word)) {
                /*
                dodon -> substring(0,2) -> dod
                string = dod + *** => dod***
                 */
                String wordToReplace;
                String first3Chars = word.substring(0, 3);
                wordToReplace = first3Chars + "***";
                text = text.replace(word, wordToReplace);
            }
        }
        System.out.println("Textul dupa cenzura:");
        System.out.println(text);
    }


}
