package md.convertit.lectia20.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Utilizator on 16.06.2017.
 */
public class MyFrame extends JFrame {

    private JButton jButtonPlus;
    private JButton jButtonMinus;// l-am declarat
    private JTextField jTextFieldCount;

    public MyFrame() throws HeadlessException {
        //initializam fereastra
        initFrame();
        //initializez componentele visuale
        initComponents();
        //adaug liteners
        addListeners();
    }

    private void initFrame() {
        setTitle("My Best frame!");
        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        //aici vom initializa si adauga componentele vizuale
        JPanel jPanel = new JPanel();
        //adag panel pe fereastra
        getContentPane().add(jPanel);

        //cream butonul +
        jButtonPlus = new JButton("+");//il initializez
        jPanel.add(jButtonPlus);

        //cream button -
        jButtonMinus = new JButton("-");
      //  jButtonMinus.setEnabled(false);
        jPanel.add(jButtonMinus);

        jPanel.add(new JLabel("Total click-uri"));

        //cream jtextfield
        jTextFieldCount = new JTextField();
        jTextFieldCount.setColumns(5); //nr de coloane
        jTextFieldCount.setText("0");// 0 valoare initiala
        jPanel.add(jTextFieldCount);
    }

    private void addListeners() {
        //vom adauga listeners pentru componentele noastre
        jButtonPlus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                System.out.println("am apasat +");de test
                //obtin valoare din jTextField
                String text = jTextFieldCount.getText();
                //convertesc text din String in Int
                int currentCount = Integer.parseInt(text);
                currentCount++; // postincrementare
                //convertesc numar in String ,deoarece jTextefield
                // accepta doar String-uri
                text = String.valueOf(currentCount);
                jTextFieldCount.setText(text);
            }
        });

        //adaugam lui Button Minus action listener
        jButtonMinus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("am apasat -");
            }
        });

    }

}
