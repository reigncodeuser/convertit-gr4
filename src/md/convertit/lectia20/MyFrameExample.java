package md.convertit.lectia20;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Utilizator on 16.06.2017.
 */
public class MyFrameExample {
    public static void main(String[] args) {
        //cream un obiect de tip JFrame
        JFrame frame = new JFrame();
        //setam titlu
        frame.setTitle("My First Frame");
        frame.setSize(400, 500);
        //o centram la mijloc de ecran
        frame.setLocationRelativeTo(null);
        //la click X - o inchidem
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(300, 400));

        //adaugam alte component pe fereasra
        //cream un obiect de tip JBUtton
        JButton jButtonClick = new JButton();
        jButtonClick.setText("Click me");
        //setam dimensiuni la button
        jButtonClick.setSize(20, 30);
        //creez obiect de tip ActionListener
        ActionListener listener = new MyClickListener();
        //adaug listener la buttonul meu
        jButtonClick.addActionListener(listener);
        //adaug butonul la fereastra
        //frame.add(jButtonClick); //nu adaugam direct, doar pe panel

        //cream un obiect de tip JPanel
        JPanel jPanel = new JPanel();
        //adaugam button pe panel
        jPanel.add(jButtonClick);


        frame.add(jPanel);
        //facem fereastra vizibila
        frame.setVisible(true);
    }
}
