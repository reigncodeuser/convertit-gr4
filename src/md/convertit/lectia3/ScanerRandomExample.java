package md.convertit.lectia3;

import java.util.Random;
import java.util.Scanner;

/**
 * De realizat un program care o sa calculeze suma a 2 numere.
 * Conditia este ca primul numar sa fie introduse de utilizator
 * de la tastatura, iar numarul al 2-lea sa fie ales aleator cu valori de la
 * 0 la 100
 */
public class ScanerRandomExample {

    public static void main(String[] args) {
        // afisez mesaj de salut
        System.out.println("Buna ziua");
        //rog sa itroduca de la tastatura primul numar
        System.out.println("Introduceti un numar:");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        //obtin inca un numar aleator (random)

        Random random = new Random();
        int y = random.nextInt(101);

        System.out.println("suma lui " + x + " si " + y + " este " + (x + y));
    }
}
