package md.convertit.lectia3;

/**
 * Created by Utilizator on 14.04.2017.
 */
public class JavaOperators {
    public static void main(String[] args) {
        //adunarea
        int x = 10;
        int y = 15;
        int rezultat = x + y;//adunarea
        System.out.println("rezultat este " + rezultat);

        int rezultatMinus = x - y;
        //1 declarare
        //2 evaluare  operator
        //3 operatia propriuzisa
        // 4 atribuire rezultat
        System.out.println("rezultat scadere: " + rezultatMinus);

        int rezultatInmultire = x * y;
        System.out.println("rezultat inmultire: " + rezultatInmultire);

        //pierdere precizie
        int rezultatImpartire = x / y;
        System.out.println("rezultat impartire: " + rezultatImpartire);

        //pierdere precizie
        double rezultImpartireDouble = x / y;
        System.out.println("rezultImpartireDouble: " + rezultImpartireDouble);

        //pentr a pastra precizia, variabilel cu care se fac operatiile
        //trebuie sa fie tot de tip-ul rezultatului
        double doubleX = 10;
        double doubleY = 15;
        System.out.println("impartire double: " + (doubleX / doubleY));

        int rezultModul = x % y; //modul operator
        System.out.println("rezultModul " + rezultModul);

        System.out.println("modulus 3 % 2= " + (3 % 2));
        System.out.println("modulus 4 % 2= " + (4 % 2));
    }
}
