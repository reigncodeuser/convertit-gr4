package md.convertit.lectia3;

/**
 * Created by Utilizator on 14.04.2017.
 */
public class BooleanExample {
    public static void main(String[] args) {
        //declaram o variabila de tip boolean
        boolean isFriday;
        //initializam variabila booleana
        isFriday = true;
        System.out.println("valoare lui isFriday este: " + isFriday);

        //declaram si initializam o variabila booleana
        boolean isMonday = false;
        System.out.println("valoarea lui isMonday este: " + isMonday);

        String myText = "Aici avem un text";
        System.out.println(myText);
        myText = "aici avem alt text";
        System.out.println(myText);
    }
}
