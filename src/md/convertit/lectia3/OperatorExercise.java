package md.convertit.lectia3;

import javax.swing.*;
import java.util.Scanner;

/**
 * Created by Utilizator on 14.04.2017.
 */
public class OperatorExercise {

    public static void main(String[] args) {
        System.out.println("Buna ziua!");
        System.out.println("Va rugam sa introduceti valoare variabilei X");

        // importam clasa Scanner, care este specializata in citirea datelor
        //din diferete surse
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        System.out.println("Valoarea lui X este: " + x);

        System.out.println("Introduceti valoare lui Y");
        int y = scanner.nextInt();
        System.out.println("Valoarea lui Y este: " + y);

        int result = x + y;
        System.out.println("Suma dintre " + x + " si " + y + " este: " + result);
    }

}
