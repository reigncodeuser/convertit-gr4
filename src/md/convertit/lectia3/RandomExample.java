package md.convertit.lectia3;

import java.util.Random;

/**
 * Created by Utilizator on 14.04.2017.
 */
public class RandomExample {
    public static void main(String[] args) {
        //cream o variabila de tip Random
        Random rand = new Random();

        int xAleator = rand.nextInt();
        System.out.println("Numar aleator al Dvs. este: " + xAleator);

        //refolosim variabila xAleator pentr a stoca un nou numar
        xAleator = rand.nextInt(11);
        System.out.println("Numar aleator al Dvs. este: " + xAleator);

        boolean randomBoolean = rand.nextBoolean();
        System.out.println("randomBoolean este: " + randomBoolean);
    }
}
