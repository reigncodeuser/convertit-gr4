package md.convertit.lectia9;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Utilizator on 08.05.2017.
 */
public class StudentCatalog {

    public static void main(String[] args) {
        //afisam un mesaj de salut
        JOptionPane.showMessageDialog(null, "Bine ati venit in registru de stat");

        //cream o lista (arraylist) in care vom stoca datele introduse
        ArrayList registru = new ArrayList();

        //mai jos o sa rugam utilizatorul sa introduca un numar nedefinit
        //de studenti

        while (true) {
            String userInput = JOptionPane.showInputDialog("Adaugati studentul");
            //adaug in registr, inregistrarea utilizatorului
            registru.add(userInput);

            //intrebam utilizatorul daca doreste sa mai introduca o inregistrare
            //daca da, continuam, daca nu -> oprim ciclul : break
            int userChoise = JOptionPane.showConfirmDialog(null,"Mai adaugam ?");
            if(userChoise==JOptionPane.NO_OPTION){
                break;
            }
        }
        //aratam lista la consola
        System.out.println(registru);
    }
}
