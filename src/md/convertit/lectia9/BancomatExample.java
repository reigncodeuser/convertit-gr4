package md.convertit.lectia9;

import javax.swing.*;

/**
 * Created by Utilizator on 08.05.2017.
 */
public class BancomatExample {

    private static final int PIN_CODE = 1111;//final -> constanta
    private static final int AVAILABLE_TRYES = 2;

    public static void main(String[] args) {
        int numarIncercari = 0;

        //afisam mesaj de salut
        JOptionPane.showMessageDialog(null, "Bine ai venit la bancomat!");

        while (numarIncercari < AVAILABLE_TRYES) {
            //in interior la while, operam cu bancomatul
            //mai intii solicitam PIN

            String userPin = JOptionPane.showInputDialog("PIN-ul Dvs ?");
            //convertim userPin din String in Int
            int pin = Integer.valueOf(userPin); //Integer.parseInt(userPin);
            //verificam daca coincide PIN-ul
            if (pin != PIN_CODE) {
                //incrementez numar de incercari
                numarIncercari++;

                //verifica daca a introdus de 3 ori, daca da, opresc program
                if (numarIncercari == AVAILABLE_TRYES) {
                    JOptionPane.showMessageDialog(null, "Ati depasit numar incercari");
                    System.exit(0);
                }

                //il trimit la introducere PIN
                System.out.println("pin ii gresit");
                continue; //incepe ciclul din nou, ignora codul de mai jos
            } else {
                //afisez operatiile
                //pregatesc optiunile disponibile
                String[] options = {"Extrage Bani", "Vezi soldul", "Iesire"};
                //afisam dialogul
                int option =
                        JOptionPane.showOptionDialog(null,
                                "Alegeti o optiune",
                                "Optiuni",
                                JOptionPane.DEFAULT_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                options,
                                options[0]);

                //vedem ce a selectat utilizatorul
                if (option == 0) {
                    //trebuie sa extraga
                    System.out.println("aici o sa extragem bani");
                } else if (option == 1) {
                    System.out.println("Aveti 1 mln de lei");
                } else {
                    System.out.println("iesim din bancomat");
                    System.exit(0);
                }

                //dupa executarea unei operatii,il intrebam daca continuma ?
                int userChoise = JOptionPane.showConfirmDialog(null, "Inca o operatie ?");
                if (userChoise == JOptionPane.NO_OPTION ||
                        userChoise == JOptionPane.CANCEL_OPTION ||
                        userChoise == JOptionPane.CLOSED_OPTION) {
                    System.out.println("iesim din bancomat");
                    System.exit(0);
                }
            }
        }
    }
}
