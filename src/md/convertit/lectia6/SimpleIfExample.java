package md.convertit.lectia6;

/**
 * Created by Utilizator on 28.04.2017.
 */
public class SimpleIfExample {

    public static void main(String[] args) {

        boolean afaraPloua = true;
        // if( <exppresieBooleana> {  cod executabil }  )

        System.out.println("***** pana la if");
        if (afaraPloua) {
            System.out.println("Trebuie sa iai umbrela");
        }
        System.out.println("**** Dupa if ");
    }

}

