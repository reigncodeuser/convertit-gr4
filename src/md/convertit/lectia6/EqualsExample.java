package md.convertit.lectia6;

/**
 * Created by Utilizator on 28.04.2017.
 */
public class EqualsExample {

    public static void main(String[] args) {
        String s1 = "mama"; // obiect se creaza in stack
        String s2 = new String("mama"); //obiect se creaza in heap
        String s3 = "mama";
        System.out.println(s1 == s2); //comparam daca
        //referintele sunt egale, si nu sunt, deoarece primul sa creat
        //in stack, al 2lea in heap
        System.out.println("s3==s1 ? : " + (s3==s1));
        // equals() -> compara daca obiectele (valoarea lor este egala)
        System.out.println(s1.equals(s2));
    }
}
