package md.convertit.lectia6;

/**
 * Created by Utilizator on 28.04.2017.
 */
public class BidimensionalArray {

    public static void main(String[] args) {
        int[][] myArray = new int[2][2];
        System.out.println("pe adresa [0][0] avem: " + myArray[0][0]);
        myArray[1][0] = 50;
        System.out.println("pe adresa [1][0] avem: " + myArray[1][0]);

        int axaI = 1;
        int axaJ = 1;
        System.out.printf("Pe axa [%d][%d] avem valoarea %d\n", axaI, axaJ, myArray[axaI][axaJ]);
        // System.out.println("Pe axa [" + axaI + "][" + axaJ + "] avem valoarea " + myArray[axaI][axaJ]);

        //schimbam valoarea de pe pozitia [1][1] din myArray
        myArray[axaI][axaJ] = 50;
        System.out.printf("Pe axa [%d][%d] avem valoarea %d\n", axaI, axaJ, myArray[axaI][axaJ]);
    }
}
