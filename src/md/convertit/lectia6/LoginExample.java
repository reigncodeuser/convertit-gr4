package md.convertit.lectia6;

import javax.swing.*;

/**
 * Created by Utilizator on 28.04.2017.
 */
public class LoginExample {
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Va rugam sa va autorizati");
        //realizam un program care va autoriza utilizatorul
        //dupa login si parola sa
        final String LOGIN = "admin";
        final String PASSWORD = "123Admin";
        //o sa rugam utilizatorul sa se autorizeze in sistem
        //dupa login si parola
        //solicitam user login
        String userLogin = JOptionPane.showInputDialog("Login please!");
        String userPass = JOptionPane.showInputDialog("User password!");

        //daca user login si user pass coincide cu login si parola din
        //sistem, autorizam accesul
        boolean hasAccess = userLogin.equals(LOGIN) && userPass.equals(PASSWORD);

        if(hasAccess){
            //codul de aici se executa numai in caz cand hasAccess == true
            JOptionPane.showMessageDialog(null,"Aveti access la banii DVS");
        }else { //else : altfel, pentru celelalte cazuri
            JOptionPane.showMessageDialog(null,"Access Interzis","NEAUTORIZAT",JOptionPane.ERROR_MESSAGE);
        }

        JOptionPane.showMessageDialog(null, "La revedere");
    }
}
