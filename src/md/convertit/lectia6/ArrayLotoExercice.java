package md.convertit.lectia6;

import java.util.Scanner;

/**
 * Created by Utilizator on 28.04.2017.
 */
public class ArrayLotoExercice {
    public static void main(String[] args) {

//        int[] simpleArray = {100,43,55}; simple array
        //cream un array bidimensional cu premii
        int[][] myArray = {
                {100, 50, 0}, //rand 0
                {-100, 0, 100} //rand 1
        };

        //rugam utilizator sa introduca 2 numere de la tastatura
        // primul cu valorile:  1 sau 2
        //al 2lea cu valorile 1, 2 sau 3
        Scanner scanner = new Scanner(System.in);
        System.out.println("Alegeti primul numar (1 sau 2)");
        int randSelectat = scanner.nextInt();

        System.out.println("Alegeti al 2lea numar (1, 2 sau 3)");
        int coloanaSelectata = scanner.nextInt();

        //scadem numarul selectat de utilizator cu -1,pentru
        //a obtine index-ul in array
        //  int newRand = randSelectat -1; //o posibilitate
        // randSelectat = randSelectat - 1; // alta posibilitate
        randSelectat--; //echivalent cu randSelectat = randSelectat - 1;
        coloanaSelectata--;
        int valoarePremiu = myArray[randSelectat][coloanaSelectata];
        System.out.printf("Felicitari, ati castigat %d $!", valoarePremiu);
    }
}
