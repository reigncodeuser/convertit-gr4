package md.convertit.lectia14;

/**
 * Created by Utilizator on 29.05.2017.
 */
public class ProdusElectronic extends Produs {

    private int putereKw;

    @Override //suprascrim metoda
    public int getPretRaft() {
        int pretCurent = super.getPret();//iau pret din clasa super
        return pretCurent + 10;
    }

    public int getPutereKw() {
        return putereKw;
    }

    public void setPutereKw(int putereKw) {
        this.putereKw = putereKw;
    }
}
