package md.convertit.lectia14;

/**
 * Created by Utilizator on 29.05.2017.
 */
public class InheritanceTest {

    public static void main(String[] args) {
        //cream un obiect de tip Masina
        Masina masina = new Masina();
        masina.setModel("Volvo");
        masina.setYear(2017);
        System.out.println(masina);

        //cream o instanta a clasei MasinaTeren
        MasinaTeren mt = new MasinaTeren();
        mt.setModel("Mitsubitsi"); //mostenite din Masina
        mt.setYear(2015);//mostenite din Masina
        mt.setOffroad(true); //cimp prezent in clasa MasinTeren
        System.out.println(mt);
    }

}
