package md.convertit.lectia14;

/**
 * Created by Utilizator on 29.05.2017.
 */
public class Produs {
    private String nume;
    private int pret;//pret net

    public int getPretRaft() {
        int pretRaft = this.pret + 5;
        return pretRaft;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }
}
