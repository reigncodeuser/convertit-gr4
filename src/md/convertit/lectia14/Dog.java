package md.convertit.lectia14;

/**
 * Created by Utilizator on 29.05.2017.
 */
public class Dog extends Animal{
    private String kind; //tip, rasa

    public Dog(String name,String kind) {
        super(name);
        this.kind=kind;
        System.out.println("suntem in constructor al clasei Dog");
    }

    public String getKind() {

        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }
}
