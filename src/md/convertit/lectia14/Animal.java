package md.convertit.lectia14;

/**
 * Created by Utilizator on 29.05.2017.
 */
public class Animal {
    private String name;

    public Animal(String name) {
        System.out.println("suntem in constructor al clasei Animal");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
