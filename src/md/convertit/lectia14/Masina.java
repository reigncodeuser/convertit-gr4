package md.convertit.lectia14;

/**
 * Created by Utilizator on 29.05.2017.
 */
public class Masina extends Object { //clasa Object este
    //mostenita implicit
    private String model;
    private int year;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Masina{");
        sb.append("model='").append(model).append('\'');
        sb.append(", year=").append(year);
        sb.append('}');
        return sb.toString();
    }
}
