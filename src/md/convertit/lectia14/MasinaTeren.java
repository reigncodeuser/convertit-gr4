package md.convertit.lectia14;

/**
 * Created by Utilizator on 29.05.2017.
 * MasinaTeren este tot  o masina, doar ca mai are un field (cimp)
 * Boolean offroad;
 */
public class MasinaTeren extends Masina {
    private boolean offroad;

    public boolean isOffroad() {
        return offroad;
    }

    public void setOffroad(boolean offroad) {
        this.offroad = offroad;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MasinaTeren{");
        sb.append("offroad=").append(offroad);
        sb.append('}')
                .append(super.toString()); //super.toString apeleaza
        //metoda toString din clasa Masina
        return sb.toString();
    }
}
