package md.convertit.lectia14;

import md.convertit.lectia12.model.Car;

/**
 * Created by Utilizator on 29.05.2017.
 */
public class ProdusTest {
    public static void main(String[] args) {
        //cream un obiect de tip produs
        Produs produs = new Produs();
        produs.setNume("Chefir");
        produs.setPret(10);
        System.out.printf("%s cu pret net %d la raft costa %d \n",
                produs.getNume(), produs.getPret(), produs.getPretRaft());

        ProdusElectronic pe = new ProdusElectronic();
        pe.setNume("Bec 12W");
        pe.setPret(10);
        pe.setPutereKw(1);
        System.out.printf("%s cu pret net %d la raft costa %d \n",
                pe.getNume(), pe.getPret(), pe.getPretRaft());

/*
Putem folosi ca referinta pentru
 obiectele noastre,si tipul super clasei
 */
        Produs p = new ProdusElectronic();
        p.setNume("Frigider");
        p.setPret(6000);
     //   p.setPutereKw(1); deoarece p are ca referinta tipul
        //clase super in care nu este cimpul putere, nu putem seta
        ProdusElectronic deConvertit = (ProdusElectronic) p;
        //dib tip mai complex, in tip mai simplu trebuie conversie
        //explicita
        System.out.printf("%s cu pret net %d la raft costa %d \n",
                deConvertit.getNume(), deConvertit.getPret(), deConvertit.getPretRaft());

        Produs ps = pe; //conversie implicita
        System.out.printf("%s cu pret net %d la raft costa %d \n",
                ps.getNume(), ps.getPret(), ps.getPretRaft());

    }
}
