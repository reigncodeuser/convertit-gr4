package md.convertit.lectia12.main;

import md.convertit.lectia12.model.Car;
import md.convertit.lectia12.model.Driver;

/**
 * Created by Utilizator on 12.05.2017.
 */
public class MyGarage {
    public static void main(String[] args) {
        //creez un obiect de tip Driver
        Driver driver = new Driver();
        //setam atribute
        driver.setName("Uncle John");
        driver.setExperience(25);

        //creez un obiect de tip Car
        Car c1 = new Car();
        c1.setModel("T1");
        c1.setYear(2017);

        //setez obiect de tip Car ca un camp al obiectului Driver
        driver.setCar(c1);
        //    System.out.println(driver.toString());
        System.out.println(driver); //automat apeleaza .toString()

        //afisam anul pentru masina Driver-ului
        //ca sa arat anul, am nevoie de referinta masinii
        Car theCar = driver.getCar(); //theCar == car
        int year = theCar.getYear();
        System.out.printf("Anul masinii este: %d\n", year);
        System.out.printf("Anul masinii este: %d\n",
                driver.getCar().getYear());
        //setam anul lui theCar - in 2018
        theCar.setYear(2018);
        System.out.printf("c1 an: %d\n", c1.getYear());
        System.out.printf("Driver car year: %d\n", driver.getCar().getYear());
        System.out.printf("c1 == theCar ?: %s\n ", (c1 == theCar));

        Car myCar = new Car("Mustang");
        myCar.setYear(2017);
        System.out.println(myCar.toString());

    }
}
