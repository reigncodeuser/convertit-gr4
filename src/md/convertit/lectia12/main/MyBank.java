package md.convertit.lectia12.main;

import md.convertit.lectia12.model.BankAccount;

/**
 * Created by Utilizator on 12.05.2017.
 */
public class MyBank {
    public static void main(String[] args) {
        //cream un obiect de tip BankAccount
        BankAccount ba = new BankAccount();
        //ba.owner = "Mister John";
        ba.setOwner("Mister John");
     /*   System.out.printf("owner: %s, number: %s , ammount : %f\n",
                ba.getOwner(),
                ba.getNumber(),
                ba.getAmount());*/
        //afisez informatia scurta despre cimpurile din obiect
        //dar mai intii trebuie sa generez metoda
        System.out.println(ba.toString());
        ba.setNumber("abc-123");
        ba.setAmount(55000D);
        System.out.println(ba.toString());

        //afisam numai numele proprietarului
        System.out.printf("Owner name: %s\n", ba.getOwner());
        System.out.printf("Account number: %s\n", ba.getNumber());
        System.out.printf("Account ammount: %.2f\n", ba.getAmount());
    }
}
