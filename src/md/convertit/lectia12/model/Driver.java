package md.convertit.lectia12.model;

/**
 * Created by Utilizator on 12.05.2017.
 */
public class Driver {
    private String name;
    private int experience;
    private Car car;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Driver{");
        sb.append("name='").append(name).append('\'');
        sb.append(", experience=").append(experience);
        sb.append(", car=").append(car);
        sb.append('}');
        return sb.toString();
    }
}
