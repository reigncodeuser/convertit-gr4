package md.convertit.lectia12.model;

/**
 * Created by Utilizator on 12.05.2017.
 */
public class BankAccount {
    private String owner;
    private String number;
    private double amount;

    //getter pentru cimpul owner:
    //  getOwner() - metoda de acces a cimpului owner
    public String getOwner() {
        return this.owner;
    }

    //setOwner(String newValue) --setter
    public void setOwner(String newValue) {
        this.owner = newValue;
    }

    public String getNumber() {
//        return number; o sa afisam numai ultimele 3 cifre
        //aflam cate litere trebuie inlocuite cu *
        int stars = this.number.length() - 3;
        String toReplace = this.number.substring(0, stars);
        String newString = this.number.replace(toReplace, "*****");
        return newString;
    }

    public void setNumber(String number) {
        //number este o variabila locala, vizibila doar
        //in interiorul metodei respective
        //this.number -> cimp din obiectul nostru
        this.number = number;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

 /*   public String toString() {
        return String.format("owner: %s, number: %s , ammount : %f\n",
                this.owner,
                this.number,
                this.amount);
    }*/

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BankAccount{");
        sb.append("owner='").append(owner).append('\'');
        sb.append(", number='").append(number).append('\'');
        sb.append(", amount=").append(amount);
        sb.append('}');
        return sb.toString();
    }
}
