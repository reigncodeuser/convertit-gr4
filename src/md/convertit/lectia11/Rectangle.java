package md.convertit.lectia11;

/**
 * Created by Utilizator on 10.05.2017.
 */
public class Rectangle {
    float x; //latura x
    float y; //latura y

    //<type> methodName(<type1> param1, <type2 param2>){
//  aici este corpul metode
//    return  <valoarea>;
// }
    //aici are declararea metodei
    float getArea() {
        //area: este o variabila locala, vizibila doar in interiorul metodei
        //getAre()
        float area = this.x * this.y;
        return area;
    }

}

