package md.convertit.lectia11;

/**
 * Created by Utilizator on 10.05.2017.
 */
public class StringUtilExample {
    public static void main(String[] args) {
        String original = "nazism";

        //mai jos metoda 'cenzureaza' a fost apelata prin referinta unui obiect
/*        StringUtil su = new StringUtil();
        String cenzurat = su.cenzureaza(original);
        System.out.println(cenzurat);*/

//deoarece este o metoda static, ea poate fi apelata prin numele Clasei
        //adica nu este nevoie sa creez un obiect
        String cenzurat = StringUtil.cenzureaza(original);
        System.out.printf("%s dupa cenzura este: %s\n",original,cenzurat);

        System.out.println("Inainte de invocare metoda sayHello();");
        //apelam metoda static sayHello() din clasa StringUtil
        StringUtil.sayHello();
        System.out.println("Dupa invocare metoda sayHello();");

        System.out.println("**********");
        System.out.println("Inainte de invocare met. sayHello(String name)");
        StringUtil.sayHello("Denis");//se executa cod din rand 26, clasa StringUtil
        System.out.println("Dupa  invocare met. sayHello(String name)");
    }
}
