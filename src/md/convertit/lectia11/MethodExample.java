package md.convertit.lectia11;

/**
 * Created by Utilizator on 10.05.2017.
 */
public class MethodExample {
    public static void main(String[] args) {

        //cream un obiect de tip Rectangle
        Rectangle rectangle1 = new Rectangle();
        //setam valori pentru x si y
        rectangle1.x = 10;
        rectangle1.y = 2.0F;
        //area -> x * y
      /*  float area = rectangle1.x * rectangle1.y;
        System.out
                .printf("Dreptunghi cu laturile %.2f si %.2f are area de %.2f\n",
                        rectangle1.x, rectangle1.y, area);
        area = rectangle1.getArea();
        System.out
                .printf("Dreptunghi cu laturile %.2f si %.2f are area de %.2f\n",
                        rectangle1.x, rectangle1.y, area);
                        */
        System.out
                .printf("Dreptunghi cu laturile %.2f si %.2f are area de %.2f\n",
                        rectangle1.x, rectangle1.y, rectangle1.getArea());

        Rectangle rectangle2 = new Rectangle();
        rectangle2.x=5;
        rectangle2.y=4;
        System.out
                .printf("Dreptunghi cu laturile %.2f si %.2f are area de %.2f\n",
                        rectangle2.x, rectangle2.y, rectangle2.getArea());

        //apelez medota getArea() in mod static (prin numele clasei, nu al obiectului)
       // Rectangle.getArea();
    }
}
