package md.convertit.lectia11;

/**
 * Created by Utilizator on 10.05.2017.
 */
public class CylinderExercises {
    public static void main(String[] args) {
        //    Cylinder.getArea(); apel in mod static este gresit, deoarece
        //metoda getArea() este non-statica, inseamna ca trebuie
        // sa o apelez prin intermediul unui obiect

        //creez un obiect de tip Cylinder
        Cylinder c1 = new Cylinder();
//        c1.radius = 10.0F;
//        c1.height = 20.0F;
        System.out.println("inainte de calcul area cilindru");
        double area = c1.getBaseArea();
        System.out.printf("area cilindru este %.2f\n", area);
        System.out.println("dupa calcul area cilindru");

        System.out.println("**********************");
        System.out.println("inainte de calcul volum");
        double volum = c1.getVolume();
        System.out.printf("volum cilindru: %.2f\n",volum);
        System.out.println("dupa calcul volum");
    }
}

