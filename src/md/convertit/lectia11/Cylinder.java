package md.convertit.lectia11;

/**
 * Created by Utilizator on 10.05.2017.
 */
public class Cylinder {
    float radius; //raza cercului
    float height; //inaltimea cilindrului

    //metoda de calcul a ariei cercului
    double getBaseArea() {
        // 3.14 * r * r
        double baseArea = Math.PI * Math.pow(this.radius, 2);
        System.out.println("suntem in getBaseArea()");
        return baseArea;
    }

    double getVolume() {
        // baseArea * h
        double baseArea = this.getBaseArea();
        double volume = baseArea * this.height;
        System.out.println("suntem in getVolume()");
        return volume;
    }
}
