package md.convertit.lectia11;

/**
 * Created by Utilizator on 10.05.2017.
 */
public class StringUtil {

    // citi paraetri ?  1 parametru de tip String
    // tipul de returnare ?  String
    //numele metodei ? cenzureaza
    static String cenzureaza(String originalWord) {
        //primile 3 originale, + ****
        String first3 = originalWord.substring(0, 3);
        first3 = first3 + "****";
        return first3;
    }

    //fiind static poate fi apelata prin numele clasei
    static void sayHello() {
        System.out.println("Hello!!! sunt in interiorul metodei sayHello()");
        //return; optional
    }

    // method overloading -> supraincarcarea metodele
    //avem 2 metode sayHello, cu parametri diferiti, prima 0, a 2a , 1 param
    static void sayHello(String name) {
        System.out.printf("Hell %s!!! sunt in interiorul metodei sayHell() cu un param\n",name);
    }


}
