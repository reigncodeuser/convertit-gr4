package md.convertit.lectia17;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class ListExercises {
    public static void main(String[] args) {
        Integer[] numbersArray = {1, 2, 5, 1, 10, 12, 33, 1, 2, 5};
        //convertim array in lista
        List<Integer> numbers = Arrays.asList(numbersArray);
        //de gasit index pe care se afla numarul 12
        int indexDeCautat = -1;
        for (int i = 0; i < numbers.size(); i++) {
            int tempValue = numbers.get(i);
            if (tempValue == 12) {
                indexDeCautat = i;
                //oprim iteratiile
                break;
            }
        }
        System.out.printf("12 a fost gasit pe index %d", indexDeCautat);
    }
}
