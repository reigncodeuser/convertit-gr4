package md.convertit.lectia17;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class MapDynamicCounter {
    public static void main(String[] args) {
        String text = "azi e soare si afara e foarte cald";
//        Map<Character, Integer> map = new HashMap<>();
        Map<Character, Integer> map = new LinkedHashMap<>();//pastreaza ordinea adaugarii
        for (int i = 0; i < text.length(); i++) {
            char tempChar = text.charAt(i);
            //verificam daca avem asa keie,
            //daca nu avem, o adaugam cu valoarea 0.
            //daca deja contine asa keie, incrementam valoarea
            if (!map.containsKey(tempChar)) {
                map.put(tempChar, 1);
            } else {
                //extrag valoare curenta
                //incrementez
                //o pun in map pentru acesi cheie
                int currentValue = map.get(tempChar);
                currentValue++;
                map.put(tempChar, currentValue);
            }
        }
        System.out.println("********");
        //obtinem lista de key (set-ul de chei)
        Set<Character> keys = map.keySet();
        for (char temChar : keys) {
            int value = map.get(temChar);//valoare pentru keia tempChar
            System.out.printf("Litera %s se repeta de %d ori\n", temChar, value);
        }
    }
}
