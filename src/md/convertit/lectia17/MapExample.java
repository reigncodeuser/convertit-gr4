package md.convertit.lectia17;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class MapExample {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        //adaugarea obiectele in map: .put()
        map.put(903, "Pompieri");
        map.put(901, "Ambulanta");
        map.put(112, "Centru Unic");
        //marimea map-ului
        System.out.printf("Marime map: %d\n", map.size());
        String value = map.get(901);
        System.out.printf("Pentru cheia 901 avem valoarea: %s\n", value);
        System.out.printf("Pentru cheia 112 avem valoarea: %s\n", map.get(112));
        map.clear();
        System.out.printf("dupa clear avem %d elemente", map.size());
    }
}
