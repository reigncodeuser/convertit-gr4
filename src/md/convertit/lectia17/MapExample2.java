package md.convertit.lectia17;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class MapExample2 {
    public static void main(String[] args) {
        String txt = "Eu ma duc la scoala";
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < txt.length(); i++) {
            char tempChar = txt.charAt(i);
            if (tempChar == 'a') {
                //verific de cate ori a fost deja char: a
                int totalRepetari = map.getOrDefault('a', 0);
                //daca nu gaseste nici o valoare pentru keia mea,
                //inseamna ca a fost gasita de 0 ori pana acum
                map.put('a', ++totalRepetari);
            }
        }
        System.out.printf("Litera 'a' se repeta de %d ori", map.get('a'));
    }
}
