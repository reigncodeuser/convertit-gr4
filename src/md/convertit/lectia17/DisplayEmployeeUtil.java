package md.convertit.lectia17;

import md.convertit.lectia17.model.Employee;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 08.06.2017.
 */
public class DisplayEmployeeUtil {

    public static List<Employee> getEmployeesBetween(List<Employee> list) {
        List<Employee> newList = new ArrayList<>();
        for (Employee temp : list) {
            if (temp.getBirthDay().getYear() > 1980
                    && temp.getBirthDay().getYear() < 1900) {
                newList.add(temp);
            }
        }
        return newList;
    }
}
