package md.convertit.lectia17.model;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class Address {
    //variabila statica este accesibila tutoror obiectelor
    public static long lastId = 0;

    private long id;
    private String streetName;
    private String number;

    private Address() {
        //incrementam lastId
        //setam ID pentru obiect creat
        lastId++;
        this.id = lastId;
    }

    public Address(String streetName, String number) {
        this(); //apelez contructor cu 0 parametri
        this.streetName = streetName;
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Address{");
        sb.append("id=").append(id);
        sb.append(", streetName='").append(streetName).append('\'');
        sb.append(", number='").append(number).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
