package md.convertit.lectia17.model;

import java.time.LocalDate;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class Employee {
    //variabila statica este accesibila tutoror obiectelor
    public static long lastId = 0;

    private long id;
    private String fullName;
    private LocalDate birthDay;
    private Address address;

    private Employee() {
        lastId++;
        this.id = lastId;
    }

    public Employee(String fullName, LocalDate birthDay, Address address) {
        this();
        this.fullName = fullName;
        this.birthDay = birthDay;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
