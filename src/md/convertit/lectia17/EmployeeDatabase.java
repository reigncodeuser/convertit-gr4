package md.convertit.lectia17;

import md.convertit.lectia17.model.Address;
import md.convertit.lectia17.model.Employee;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class EmployeeDatabase {

    private static List<Address> addresses;
    private static List<Employee> employees;

    //bloc static - care se executa inainte constructorului
    {
        System.out.println("se incarca blocul static");
/*        addresses = new ArrayList<>();
//        addresses.add(new Address()); constructor privat
        addresses.add(new Address("Ismail", "11/2"));
        addresses.add(new Address("Stefan cel Mare", "23a"));
        System.out.println(addresses);*/


    }

    //metoda publica de access a listei de employees
    public static List<Employee> getEmployees() {
        employees = new ArrayList<>();
        Employee employee = new Employee("John Doe",
                LocalDate.of(1990, 1, 1), new Address("Ismail", "11/2"));
        //adauga employee in lista
        employees.add(employee);
        //todo de adaugat in 10 employee
        return employees;
    }
}
