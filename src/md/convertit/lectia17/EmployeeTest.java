package md.convertit.lectia17;

import md.convertit.lectia17.model.Employee;

import java.util.List;

/**
 * Created by Utilizator on 05.06.2017.
 */
public class EmployeeTest {
    public static void main(String[] args) {
        //obtin o referinta catre lista de employee
        List<Employee> list = EmployeeDatabase.getEmployees();
        //new EmployeeDatabase().getEmployees(); creaza copie in memorie - nu e necesar
        System.out.printf("Total angajati: %d\n", list.size());

        /*
        Stocam intr-o lista noua toti angajatii care sau nascut intre anii  1980 si 1990
//creez o lista noua goala
    */
//        List<Employee> newList = new ArrayList<>();
        List<Employee> newList = DisplayEmployeeUtil
                .getEmployeesBetween(list);

        /*
        //ii afisam la consola folosind for i (for-ul cu index)

        Stocam intr-o lista noua toti angajatii care traiesc pe strada Ismail

        //ii afisam la consola folosind foreach

        Stocam intr-o lista toti angajatii care full name se incepe cu I

        //ii afisam intr-o lista folosind iterator


        Apoi mutam toate metodele de afisare intr-o clasa DisplayEmployeeUtil si le facem statice dupa care modificam
        programul ca sa foloseasca metodele statice din clasa DisplayEmployeeUtil
         */
    }
}
