package md.convertit.lectia15;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class AbstractProductTest {
    public static void main(String[] args) {
        //  AbstractProduct abstractProduct = new AbstractProduct();
        //cream un obiect de tip MD Product
        MDProduct mdProduct = new MDProduct();
        mdProduct.setBrutPrice(100D);//setBrutPrice -> mostenita din cl. abstracta
        //apelam metoda getPriceWithTax
        System.out.printf("MDProduct: la pret brut %.2f avem cu taxe %.2f\n",
                mdProduct.getBrutPrice(), mdProduct.getPriceWithTax());

        //cream un obiect de tip EUProduct
        EUProduct euProduct = new EUProduct();
        euProduct.setBrutPrice(100D);
        System.out.printf("EUProduct: la pret brut %.2f avem cu taxe %.2f\n",
                euProduct.getBrutPrice(), euProduct.getPriceWithTax());

        //cream obiecte folosind ca referinta clasa abstracta
        AbstractProduct ap = new MDProduct();
        ap.setBrutPrice(50D);
        System.out.printf("AP: la pret brut %.2f avem cu taxe %.2f\n",
                ap.getBrutPrice(), ap.getPriceWithTax());
        ap = euProduct; //ap ->atribui referinta unui obiect concret
        System.out.printf("AP: la pret brut %.2f avem cu taxe %.2f\n",
                ap.getBrutPrice(), ap.getPriceWithTax());
    }
}
