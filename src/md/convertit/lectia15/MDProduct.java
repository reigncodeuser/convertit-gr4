package md.convertit.lectia15;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class MDProduct extends AbstractProduct {

    @Override
    public Double getPriceWithTax() {
        //md - taxa ii 20%
        double adaos = getBrutPrice() * 0.2;
        return getBrutPrice() + adaos;
    }
}
