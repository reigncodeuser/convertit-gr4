package md.convertit.lectia15;

/**
 * Created by Utilizator on 31.05.2017.
 */
public abstract class AbstractProduct {
    private Double brutPrice;

    public abstract Double getPriceWithTax();

    public Double getBrutPrice() {
        return brutPrice;
    }

    public void setBrutPrice(Double brutPrice) {
        this.brutPrice = brutPrice;
    }
}
