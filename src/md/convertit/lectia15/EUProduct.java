package md.convertit.lectia15;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class EUProduct extends AbstractProduct  {
    private final int RECYCLING_TAX = 2;

    @Override
    public Double getPriceWithTax() {
        //eu - taxa ii 20% + 2 euro taxa reciclare
        double adaos = getBrutPrice() * 0.2;
        return getBrutPrice() + adaos + RECYCLING_TAX;
    }
}
