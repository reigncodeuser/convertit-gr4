package md.convertit.lectia15.interfaces;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class InterfaceTest {
    public static void main(String[] args) {
        //creez un obiect de tip Car
        Car car = new Car();
        //creez o variabila folosind ca referint interfata
        AlimenteazaIntf aIntf = car;
        aIntf.seAlimenteaza();

        aIntf = new Animal();
        aIntf.seAlimenteaza();
    }
}
