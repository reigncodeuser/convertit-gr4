package md.convertit.lectia15.interfaces;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class Car implements AlimenteazaIntf{
    private String model;
    private int year;

    public Car() {
    }

    public Car(String model, int year) {
        this.model = model;
        this.year = year;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Car{");
        sb.append("model='").append(model).append('\'');
        sb.append(", year=").append(year);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void seAlimenteaza() {
        System.out.println("sunt masina si ma alimentez cu combustibil");
    }


}
