package md.convertit.lectia15.interfaces;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class Animal implements AlimenteazaIntf {

    private String kind; //specia
    private String color;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void seAlimenteaza() {
        System.out.println("eu is animal si ma alimentez cu hrana");
    }
}
