package md.convertit.lectia15.interfaces.intfExercice;

import java.util.Scanner;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class ConsoleMessage implements Message {

    @Override
    public void displayMessage(String name) {
        System.out.println("Buna ziua domnule " + name);
    }

    @Override
    public String getName() {
        System.out.println("Introduceti numele!");
     /*   Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        return name;*/
        return new Scanner(System.in).next();
    }
}
