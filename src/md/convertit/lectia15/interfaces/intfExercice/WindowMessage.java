package md.convertit.lectia15.interfaces.intfExercice;

import javax.swing.*;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class WindowMessage implements Message {

    @Override
    public void displayMessage(String name) {
        JOptionPane.showMessageDialog(null, "Buna ziua domnule " + name);
    }

    @Override
    public String getName() {
        return JOptionPane.showInputDialog("Cum va numiti!?");
    }
}
