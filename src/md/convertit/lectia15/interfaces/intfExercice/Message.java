package md.convertit.lectia15.interfaces.intfExercice;

/**
 * Created by Utilizator on 31.05.2017.
 */
public interface Message {

    void displayMessage(String name);

    String getName();
}
