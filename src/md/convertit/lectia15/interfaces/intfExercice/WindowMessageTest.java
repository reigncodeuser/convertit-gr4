package md.convertit.lectia15.interfaces.intfExercice;

import java.util.Scanner;

/**
 * Created by Utilizator on 31.05.2017.
 */
public class WindowMessageTest {

    public static void main(String[] args) {
        //1 rog utilizatorul sa selecteze metoda de afisare
        // 1- consola, 2 - fereastra
        Scanner scanner = new Scanner(System.in);
        System.out.println("alegeti metoda de afisare: 1- consola, 2-fereastra");
        int userInput = scanner.nextInt();
        Message message = null;
        String name;
        if (userInput == 1) {
            //incarc console
            message = new ConsoleMessage();
        } else if (userInput == 2) {
            //incarc fereastra
            message = new WindowMessage();
        } else {
            System.err.println("Ati selectat valoare invalida!");
            System.exit(1);
        }

        name = message.getName();
        message.displayMessage(name);


    }
}
