package md.convertit.lectia8;

/**
 * Created by Utilizator on 05.05.2017.
 */
public class WhileExample {
    public static void main(String[] args) {

        int i = 0;

        while (i < 5) {
            System.out.printf("Avem iteratia %d \n", i);
            i++;
        }
        System.out.println("******************");
        int j = 0;
        while (true) {
            System.out.printf("J are valoarea: %d\n", j);
            j++;

            //daca j == 10 , opreste ciclul
            if (j == 10) {
                System.out.println("Oprim proces la " + j);
                break;
            }
        }
    }
}
