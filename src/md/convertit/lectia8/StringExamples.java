package md.convertit.lectia8;

/**
 * Created by Utilizator on 05.05.2017.
 */
public class StringExamples {

    public static void main(String[] args) {

        String txt = "mama"; // ['m','a','m','a']
        char charAt0 = txt.charAt(0); // m - care este caracter pe index 0 ,din string 'mama'
        System.out.printf("Pe index %d avem char: %s\n",0,charAt0);
        System.out.printf("Pe index %d avem char: %s\n",1,txt.charAt(1));

        //tip valoarea returnata      nume metoda     parametru
        //int	                      codePointAt(int index)
        int codeAt1 = txt.codePointAt(1);
        System.out.printf("Pe index %d avem cod numeric: %d ",1,codeAt1);
    }

}
