package md.convertit.lectia8;

/**
 * Created by Utilizator on 05.05.2017.
 */
public class DoWhileExample {
    public static void main(String[] args) {

        //     do{ //cod executabil} while ( expresie booleana);
        do {
            System.out.println("Suntem in do!!!");
        } while (false);
        // do while -> mai intii excecuta codul , si apoi valideaza
        //expresia din while. De aici rezulta ca in cazul lui
        // do-> while codul din do se executa minim odata

        int i = 0;
        do {
            System.out.printf("I are valoare %d\n", i);
            i--;
        } while (i > -5);
    }
}
