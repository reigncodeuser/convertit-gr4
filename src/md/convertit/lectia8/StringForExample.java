package md.convertit.lectia8;

/**
 * Created by Utilizator on 05.05.2017.
 */
public class StringForExample {
    public static void main(String[] args) {
        String txt = "Mama";
        //verificam daca prima litera (index 0) e majuscula
        //extrag primul caracter
        char first = txt.charAt(0);
        //verifica daca char-ul este uppercase
        boolean isUpper = Character.isUpperCase(first);
        System.out.printf("%s este majuscula: %s", first, isUpper);

        System.out.println("***********");
        String myText = "Eu programez in Java!";
        //fac split dupa spatiu, si obtin un array de string-uri
        String[] words = myText.split(" ");
        // words[0] - Eu
        // words[1] - programez
        // words[2] - in
        // words[3] - Java
        //trebuie sa numar cate cuvine se incep cu majuscula
        int totalMajuscule = 0;//initial nu stiu cate majuscule
        for (int i = 0; i < words.length; i++) {
            //aici verific daca am majuscula
            String currentWord = words[i];
            //extract primul caracter , (de pe index 0)
            // i = 0, index = 0, currentWord= Eu, charAt(0)-> E
            char firstChar = currentWord.charAt(0);
            //verific daca firstChar e majuscula sau nu,
            //daca e majuscula, totalMajuscule se incrementeaza
            isUpper = Character.isUpperCase(firstChar);
            if (isUpper) { //echivalent cu if(isUpper==true)
                totalMajuscule++; //echivalent cu totalMajuscule= totalMajuscule+1;
            }
        }
        System.out.printf("Total majuscule gasite %d litere\n", totalMajuscule);

        System.out.println(myText.split(" ")[3]);
    }
}
