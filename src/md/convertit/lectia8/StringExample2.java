package md.convertit.lectia8;

/**
 * Created by Utilizator on 05.05.2017.
 */
public class StringExample2 {
    public static void main(String[] args) {
        String txt = "mama";
        System.out.println(txt);
        //incerc sa il transform in majuscule
        //     txt.toUpperCase(); //compilator creaza in memorie un alt obiect
        //care e identic cu primul, doar ca cu majuscule,
        //  primul obiect ramane neschimbat.

        String txtUpper = txt.toUpperCase();
        System.out.println(txtUpper);

        String txt1 = "tractor";
        //verificam daca txt1 se incepe cu "tr"
        boolean startWith = txt1.startsWith("tr");
        System.out.printf("%s se incepe cu %s: %s \n", txt1, "tr", startWith);
        System.out.printf("%s se incepe cu %s: %s \n", txt1, "TR", txt1.startsWith("TR"));

        // mama -> tata ,  creaza un obiect nou, in care 'm' este inlocuit cu 't'
        String replaced = txt.replaceAll("m", "t");
        System.out.printf("%s dupa replace m cu t avem %s\n", txt, replaced);

        //lenth - lungime string
        System.out.printf("%s are lugimea %d litere", txt, txt.length());
    }
}
