package md.convertit.lectia19;

import javax.swing.*;
import java.io.*;

/**
 * Created by Utilizator on 14.06.2017.
 */
public class ReadFromFileExample {
    public static void main(String[] args) {
        String path = "demo.txt";
        File file = new File(path);
        //citirea din fisiere se face cu interfata Reader
        try {
            //  Reader reader = new FileReader(file);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            StringBuilder builder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                //citim rand cu rand
//            textFromFile = textFromFile+line; nerecomandat
                builder.append(line);
                builder.append("\n");
            }
//            JOptionPane.showMessageDialog(null, textFromFile);
            JOptionPane.showMessageDialog(null, builder.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
