package md.convertit.lectia19;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by Utilizator on 14.06.2017.
 */
public class WriteToFileExample {
    public static void main(String[] args) {
        String path = "demo.txt";
        //cream un obiect de tip File
        File file = new File(path);
        System.out.println(file.getAbsolutePath());//calea absoluta
        System.out.println(file.getTotalSpace()); //spatiu total
        System.out.println(file.isFile()); // ii fisier ?
        System.out.println(file.exists()); // exista ?

        String text = "Acesta este un rand\n Aici este alt rand";
        //creez un obiect de tip FileWriter
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(text);//scriu text in fisier
            System.out.printf("Fisier scris: %s", file.getAbsolutePath());
            fileWriter.close(); //obligatoriu inchidem, altfel nu se scrie
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
