package md.convertit.lectia19;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Utilizator on 14.06.2017.
 */
public class ExcelReadExample {
    public static void main(String[] args) {
        String fileName = "demoexcel.xls";
        File file = new File(fileName);
        try {
            //am deschis stream din fisier
            FileInputStream fis = new FileInputStream(file);
            //cream workbook
            Workbook workbook = new HSSFWorkbook(fis);
            //obtin fila (sheet)
            Sheet sheet = workbook.getSheetAt(0);
            //obtin rand 0 din fila
            Row row = sheet.getRow(0);
            //celula 0 de pe rand 0
            Cell cell = row.getCell(0);
            System.out.printf("Row 0 cell 0: value: %s\n", cell.getStringCellValue());
            cell = row.getCell(1);
            System.out.printf("Row 0 cell 1: value: %s\n", cell.getStringCellValue());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
