package md.convertit.lectia19;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Utilizator on 14.06.2017.
 * descarcare librarie : https://mvnrepository.com/artifact/org.apache.poi/poi/3.7
 * atasare librari : click dreapta pe nume proiect -> Open Module Settings
 * -> Libraries -> +
 */
public class WriteToExcelExample {
    public static void main(String[] args) {
        //CREAM WORKBOOK
        Workbook workbook = new HSSFWorkbook();
        //CREAM SHEET (fila)
        Sheet sheet = workbook.createSheet();
        //CREAM RAND
        Row row = sheet.createRow(0); //cream primul rand
        //CREAM CELULE
        Cell cell = row.createCell(0);
        cell.setCellValue("Rind 0, celula 0");
        //folosim aceasi referinta cell pentru celula 1
        cell = row.createCell(1);
        cell.setCellValue("Rind 0, celula 1");

        //refolosim variabila row ca referinta pentru un rand nou
        row = sheet.createRow(1);
        cell = row.createCell(0);
        cell.setCellValue("Rand 1, celula 0");

        cell = row.createCell(1);
        cell.setCellValue("Rand 1 celula 1");

        //am pregatit datele in memorie ,
        //urmeaza sa le scrim in fisier
        String fileName = "demoexcel.xls";
        File file = new File(fileName);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);//am conectat un streamla fisier
            workbook.write(outputStream);//am scris in fisier
            outputStream.close();//am inchis streamul
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            System.out.println("Am scris in fisier " + file.getAbsolutePath());
        }

    }
}
