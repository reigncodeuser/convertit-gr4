package md.convertit.lectia13.main;


import md.convertit.lectia13.model.Student;

import java.time.LocalDate;

/**
 * Created by Dennis on 15.05.2017.
 */
public class StudentCatalog {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(student);
        System.out.println();

        Student s2 = new Student(1l, "Danu", "Caraganciu");
        LocalDate localDate = LocalDate.of(1987, 01, 24);
        s2.setBirthDay(localDate);
        System.out.println(s2);

        System.out.printf("Studentul %s sa nascut pe %d in luna %s a anului %d", s2.getFirstName(),
                s2.getBirthDay().getDayOfMonth(),
                s2.getBirthDay().getMonth(),
                s2.getBirthDay().getYear());
    }
}
