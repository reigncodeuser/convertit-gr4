package md.convertit.lectia13.main;


import md.convertit.lectia13.model.Student;

import java.util.ArrayList;

/**
 * Created by Dennis on 15.05.2017.
 */
public class StudentDataBase {
    public ArrayList<Student> filterByName(ArrayList list) {
        ArrayList<Student> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Student student = (Student) list.get(i);

            if (student.getLastName().startsWith("C")) {
                newList.add(student);
            }
        }
        return newList;
    }
}
