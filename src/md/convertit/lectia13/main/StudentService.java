package md.convertit.lectia13.main;


import md.convertit.lectia13.model.Student;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Dennis on 15.05.2017.
 */
public class StudentService {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        Student s1 = new Student(1L, "Igor", "Cioroi", LocalDate.of(1987, 01, 24));
        System.out.println(s1);
        System.out.println();

        list.add(s1);
        list.add(new Student(2L, "Denis", "Croitoru", LocalDate.of(1983, 11, 12)));
        list.add(new Student(2L, "Doina", "Damnic", LocalDate.of(1975, 11, 21)));
        list.add(new Student(2L, "Ion", "Croitoru", LocalDate.of(2003, 11, 22)));
        list.add(new Student(2L, "Ana", "Muncitoru", LocalDate.of(1966, 11, 14)));
        list.add(new Student(2L, "Sereoja", "Arabadji", LocalDate.of(1985, 11, 22)));
        list.add(new Student(2L, "Dima", "Croitoru", LocalDate.of(1985, 11, 22)));
        list.add(new Student(2L, "Dima", "Croitoru", LocalDate.of(2012, 11, 22)));
        list.add(new Student(2L, "Dima", "Croitoru", LocalDate.of(1985, 11, 22)));
        list.add(new Student(2L, "Dima", "Croitoru", LocalDate.of(2001, 11, 22)));
        System.out.println(list.size());
        System.out.println();

        ArrayList newList = new ArrayList();
        System.out.printf("Initial are marimea %d\n", newList.size());

        for (int i = 0; i < list.size(); i++) {
            Student student = (Student) list.get(i);
            if (student.getBirthDay().getYear() < 2000) {
                newList.add(student);

            }
        }
        System.out.printf("Total studenti nascuti pina la 2000 sunt: %d\n", newList.size());
        System.out.println();

        StudentDataBase studentDataBase = new StudentDataBase();
        ArrayList<Student> filterByNameList = studentDataBase.filterByName(list);
        System.out.printf("In lista avem: %d Studenti cu inceputul cu litera 'C iar studenti care sau nascut pina la 2000 = %d", filterByNameList.size(), newList.size());


    }
}
