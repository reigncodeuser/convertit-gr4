package md.convertit.lectia13.main;


import md.convertit.lectia13.model.Person;

import java.util.Date;

/**
 * Created by Dennis on 15.05.2017.
 */
public class PersonTest {
    public static void main(String[] args) {
        Person person = new Person();
        Person person1 = new Person(1L, "Igor", "Cioroi");
        Date date = new Date(1990, 01, 13);
        person1.setBirthDate(date);
        System.out.println(person);
        System.out.println();
        System.out.println(person1);


    }
}
