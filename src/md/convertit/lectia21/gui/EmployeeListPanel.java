package md.convertit.lectia21.gui;

import md.convertit.lectia21.model.Employee;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 19.06.2017.
 */
public class EmployeeListPanel extends JPanel {

    private JList<Employee> jList;//declaram lista
    private List<Employee> data;

    public EmployeeListPanel() {
        initData();
        initPanel();
    }

    private void initData() {
        data = new ArrayList<>();
        data.add(new Employee("Ion", "Balon", 19000D));
        data.add(new Employee("Iura", "Borta", 17000D));
        data.add(new Employee("Maria", "Magdalena", 25000D));
    }

    private void initPanel() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        //cream model
        DefaultListModel<Employee> model = new DefaultListModel<>();
        //adaug elemente in model
        model.addElement(data.get(0));
        model.addElement(data.get(1));
        model.addElement(data.get(2));
        //initializam JLIST
        jList = new JList<>();
        //setez model lui JLIST
        jList.setModel(model);
        //crez scrollPane, si transmit jList in constructor
        JScrollPane jScrollPane = new JScrollPane(jList);
        jScrollPane.setHorizontalScrollBar(new JScrollBar(JScrollBar.HORIZONTAL));
        jScrollPane.setVerticalScrollBar(new JScrollBar(JScrollBar.VERTICAL));

        //adaug scrollPane pe panel
        add(jScrollPane);
    }

    /**
     * Save a new employee in list
     *
     * @param employee to be saved
     */
    public void saveEmployee(Employee employee) {
        //trebuie sa adaug la modelul lui JList un employee nou
        //ne trebuie modeulul jlistei
        DefaultListModel<Employee> model =
                (DefaultListModel<Employee>) jList.getModel();

        //adaug employee in lista
        model.addElement(employee);
        //refresh ???
        System.out.println("am apelat saveEmployee");
    }
}
