package md.convertit.lectia21.gui;

import md.convertit.lectia21.model.Employee;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Utilizator on 19.06.2017.
 */
public class EmployeeCatalog extends JFrame {

    private final int FIELD_COLUMNS = 35;

    private JSplitPane jSplitPane;
    private JPanel leftPanel;
    private EmployeeListPanel rightPanel;
    private JTextField jTextFieldName;
    private JTextField jTextFieldLastName;
    private JTextField jTextFieldSalary;
    private JButton jButtonSave;
    private JButton jButtonClear;

    public EmployeeCatalog() throws HeadlessException {
        initWindow();
        initPane();
        initListeners();
    }

    private void initListeners() {
        //adauga action listener pentru buton save
        jButtonSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //validam forma
                boolean isFormValid = validateForm();

                //stocam obiectul de tip employee
                if (isFormValid) {
                    //salvam
                    saveEmployee();
                }
            }
        });
        jButtonClear.addActionListener(e -> {
            clearForm();
        });
    }

    private void clearForm() {
        //fac clear la nume
        jTextFieldName.setText("");
        jTextFieldLastName.setText("");
        jTextFieldSalary.setText("0.0");
    }

    //salvam obiectul
    private void saveEmployee() {
        Employee employee = new Employee();
        String lastName = jTextFieldLastName.getText();
        employee.setLastName(lastName);

        String firstName = jTextFieldName.getText();
        employee.setFirstName(firstName);

        String salaryAsText = jTextFieldSalary.getText();
        Double salary = Double.parseDouble(salaryAsText);
        employee.setSalary(salary);

        //transmit lui rightpanel pentru salvare
        rightPanel.saveEmployee(employee);
        clearForm();
    }

    private boolean validateForm() {
        boolean isValid = true;
        StringBuilder builder = new StringBuilder();
        builder.append("Au intervenit urmatoarele erori:\n");

        //validam textfields
        String firstName = jTextFieldName.getText();
        if (firstName.length() == 0 || firstName.length() > 10) {
            isValid = false;
            String message = "Prenumele trebuie sa de la 1 la 10 litere";
            builder.append(message);
            builder.append("\n");
            //  JOptionPane.showMessageDialog(null, message);
        }

        String lastName = jTextFieldLastName.getText();
        if (lastName.length() == 0 || lastName.length() > 10) {
            isValid = false;
            String message = "Numele trebuie sa de la 1 la 10 litere";
            builder.append(message);
            builder.append("\n");
            //  JOptionPane.showMessageDialog(null, message);
        }

        String salaryAsText = jTextFieldSalary.getText();
        try {
            Double.parseDouble(salaryAsText);
        } catch (NumberFormatException e) {
            isValid = false;
            builder.append("Salriul introdus este invalid");
        }

        //daca isValid ii false: afisam dialog cu eroarea
        if (!isValid) {
            JOptionPane.showMessageDialog(this, builder.toString(),
                    "Atentie", JOptionPane.ERROR_MESSAGE);
        }

        //validam salary sa fie numar7
        return isValid;
    }

    private void initPane() {
        //declaram si initializam splitPane
        jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        //trebuie sa adaug compoenent pe stanga
        leftPanel = getLeftPanel();
        //adaug leftPanel pe splitPane
        jSplitPane.setLeftComponent(leftPanel);
        //trebuie sa adaug compoenent pe dreapta
        rightPanel = new EmployeeListPanel();

        jSplitPane.setRightComponent(rightPanel);

        jSplitPane.setDividerLocation(200);
        getContentPane().add(jSplitPane); //adaug split pane pe frame
    }

    /**
     * create a form panel
     *
     * @return left panel
     */
    private JPanel getLeftPanel() {
        JPanel jPanel = new JPanel();
        //cream un obiect BOX Layout
        BoxLayout layout = new BoxLayout(jPanel, BoxLayout.Y_AXIS);
        jPanel.setLayout(layout);

        //jPanel.setBackground(Color.BLUE);
        //o sa adaugam si celelalte componente aici
        JLabel label = new JLabel("Nume");
        jPanel.add(label);

        jTextFieldName = new JTextField(FIELD_COLUMNS);
        jTextFieldName.setMaximumSize(jTextFieldName.getPreferredSize());
        jPanel.add(jTextFieldName);

        jPanel.add(new JLabel("Prenume"));
        jTextFieldLastName = new JTextField(FIELD_COLUMNS);
        jTextFieldLastName.setMaximumSize(jTextFieldLastName.getPreferredSize());
        jPanel.add(jTextFieldLastName);

        jPanel.add(new JLabel("Salary"));
        jTextFieldSalary = new JTextField(5);
        jTextFieldSalary.setMaximumSize(jTextFieldSalary.getPreferredSize());
        jPanel.add(jTextFieldSalary);

        jButtonSave = new JButton("Save");
        jPanel.add(jButtonSave);

        jButtonClear = new JButton("Clear");
        jPanel.add(jButtonClear);
        return jPanel;
    }

    private void initWindow() {
        setSize(500, 600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }
}
