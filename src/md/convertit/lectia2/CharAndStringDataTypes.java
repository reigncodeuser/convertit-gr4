package md.convertit.lectia2;

/**
 * Created by Utilizator on 12.04.2017.
 */
public class CharAndStringDataTypes {
    public static void main(String[] args) {
        //declar o variabila de tip char, numele: myChar
        char myChar = 'm';
        System.out.println("myChar are valoarea: " + myChar);

        //declaram si initializam un String
        String myText = "mama";
        System.out.println("myText are valoarea: " + myText);

        //declaram o variabila de tip char, myLetter
        //si o initilizam cu 'a'
        char myLetter = 'a';
        //pregatesc o variabila de tip int pentru a converti
        // pe myLetter in int
        int myLetterInt;
        //facem conversia
        myLetterInt = myLetter; //conversie implicita
        System.out.println(myLetter + " in int are valoarea: " + myLetterInt);
        System.out.println('A' + " in int are valoarea: " + (int)'A');

        //declaram un int, cu valoarea 66
        int myInt  = 66;
        char theChar = (char) myInt; //conversie explicita
        System.out.println(myInt + " ca char are valoarea: " + theChar);
    }
}
