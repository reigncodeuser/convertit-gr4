package md.convertit.lectia2;

/**
 * Created by Utilizator on 12.04.2017.
 */
public class VariableDeclaration {

    public static void main(String[] args) {
        //declar o variabila de tip byte
        byte myByte;
        //initilizez variabila de tip byte
        myByte = 5;
        //o afisam la consola
        System.out.println(myByte);
        //schimbam valoare variabile de myByte
        myByte = 127;
        //afisam din nou valoare variabilei myByte
        System.out.println(myByte);
        //declaram o variabila de tip int
        int currentDay;
        //initializam currentDay cu valoarea 5
        currentDay = 5;
        System.out.println("currentDay are valoare: " + currentDay);
        currentDay = 7;
        System.out.println("currentDay are valoare: " + currentDay);
        //atribui valoare din myByte lui currentDay
        currentDay = myByte;
        System.out.println("dupa atribuire lui currentDay, valoarea lui myByte avem: "
                + currentDay);
    }
}
