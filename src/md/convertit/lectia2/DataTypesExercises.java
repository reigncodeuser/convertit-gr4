package md.convertit.lectia2;

/**
 * Created by Utilizator on 12.04.2017.
 */
public class DataTypesExercises {
    public static void main(String[] args) {
        //declar o variabila de tip byte,cu numele "testByte"
        byte testByte;
        //intilizez variabila "testByte" cu valoare 10
        testByte = 10;
        //declar o variabila de tip int, numele "notaFizica"
        int notaFizica;
        //initilizez variabila "notaFizica" cu valoare 8
        notaFizica = 8;
        //afisam la consola valoarea variabilei testByte
        System.out.println(testByte);
        //afisam la consola valoarea variabilei notaFizica
        System.out.println(notaFizica);

        //declaram o variabila de tip long ,
        // numele "myLong"  cu valoarea 1000 000 ;
        long myLong;
        myLong = 1000000;
        //afisam la consola textul:
        // "Variabila myLong are valoarea 1000 000"
        System.out.println("Variabila myLong are valoarea " + myLong);
        //declaram o variabila de tip float,
        //cu numele : pretPaine
        float pretPaine;
        //initilizam "pretPaine" cu valoarea 4.6
        pretPaine = 4.6F;
        //afisam textul:
        //Pretul painii la noi este: 4.6
        System.out.println("Pretul painii la noi este: " + pretPaine);
        //declaram o variabila de tip double,
        //cu numele "salariuMediu"
        double salariuMediu;
        //initializam "salariuMediu" cu valoare 5000.56
        salariuMediu = 5000.56;

        //afisam la consola:
        //Salariu mediu in MD este 5000.56
        System.out.println("Salariu mediu in MD este " + salariuMediu);
    }
}
