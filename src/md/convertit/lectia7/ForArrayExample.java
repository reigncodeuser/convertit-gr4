package md.convertit.lectia7;

/**
 * Created by Utilizator on 03.05.2017.
 */
public class ForArrayExample {
    public static void main(String[] args) {
        String[] names = {"Ion", "Vasile", "Ana", "Ilie"};
        //ex: Ana ->  names[2]
        int length = names.length;
        for (int i = 0; i < length; i++) {
            //i coincide cu index-ul din array
            String currentValue = names[i];
            System.out.printf("Pe index %d din array avem valoarea: %s \n", i, currentValue);
        }
    }
}
