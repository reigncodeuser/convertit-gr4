package md.convertit.lectia7;

import javax.swing.*;

/**
 * Created by Utilizator on 03.05.2017.
 */
public class SwitchCaseExample {
    public static void main(String[] args) {
//        String day = "marti";
        String day = JOptionPane.showInputDialog(null, "Ce zi este azi ?");
        //convertim text introdus in text cu litere mici
        // String newText = day.toLowerCase();
        day = day.toLowerCase();
        switch (day) { //day -> varibila pentru comparari
            //mai jos tratam cazurile (case-urile) posibile
            case "luni":
                System.out.println("Ii prima zi din saptamana");
                break; //opreste executia
            case "marti":
            case "miercuri":
            case "joi": {
                System.out.println("O zi simpla de lucru");
                break;
            }
            case "vineri": {
                System.out.println("vine weekend");
                break;
            }
            case "sambata":
            case "duminica": {
                System.out.println("Ihooo, ii weekend");
                break;
            }
            default: //celelalte cazuri
                System.out.println("Ziua introdusa este gresita"); //la default nu trebuie break
        }
    }
}
