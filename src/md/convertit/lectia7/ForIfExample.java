package md.convertit.lectia7;

/**
 * Created by Utilizator on 03.05.2017.
 */
public class ForIfExample {
    public static void main(String[] args) {
        int[] numbers = {10, 3, -2, 8, -100};
        //afisez fiecare valoare la consola, cu text in dependenta
        //de fiecare , pozitiv sau nu
        for (int i = 0; i < numbers.length; i++) {
            int tempValue = numbers[i];
            //daca tempValue > 0 , afisam "Numar Pozitiv" altfel "Numar Negativ"
            if (tempValue > 0) {
                System.out.printf("pe index %d , avem valoarea %d si este POZITIV\n", i, tempValue);
            } else {
                System.out.printf("pe index %d , avem valoarea %d si este NEGATIV\n", i, tempValue);
            }
        }
    }
}
