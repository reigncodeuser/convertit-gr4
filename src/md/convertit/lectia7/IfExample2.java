package md.convertit.lectia7;

import javax.swing.*;

/**
 * Created by Utilizator on 03.05.2017.
 */
public class IfExample2 {
    public static void main(String[] args) {
        System.out.println("Care este sexul Dvs ?");
        // M - masculin , F - femenin , //valoare gresita
        String message = "";
//        String sex = "m";
        String sex = JOptionPane.showInputDialog(null, "Sexul dvs ? (M/F)");
        sex = sex.toUpperCase();

        if (sex.equals ("M")) {
            message = "Buna ziua domnule";
        } else if (sex.equals("F")) {
            message = "Buna ziua doamna!";
        } else {
            message = "Valoare gresita";
        }

        System.out.println(message);
    }
}
