package md.convertit.lectia7;

import java.util.Scanner;

/**
 * Created by Utilizator on 03.05.2017.
 */
public class ElseIfExample {
    public static void main(String[] args) {
        //cream un obiect de tip Scanner
        Scanner scanner = new Scanner(System.in);
        //rugam utilizator sa introduca nota
//        int nota = 8;
        System.out.println("Introduceti nota Dvs.");
        int nota = scanner.nextInt();
        if (nota < 5) {
            System.out.println("Esti corigent");
        } else if (nota == 5 || nota == 6) {
            System.out.println("Abia ai trecut");
        } else if (nota == 7 || nota == 8) {
            System.out.println("Destul de bun");
        } else { // 9 sau 10
            System.out.println("Excelent");
        }
    }
}
