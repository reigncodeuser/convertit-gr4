package md.convertit.lectia16;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 02.06.2017.
 */
public class ListExample {
    public static void main(String[] args) {
        List myList = new ArrayList();

        String obj1 = "un text";
        Long obj2 = 100L;
        //adaug obiectele in myList -> add()7
        myList.add(obj1);
        myList.add(obj2);
        System.out.printf("Lista are marimea %d\n", myList.size());
        //adaugam din nou obj1 in lista
        myList.add(obj1);
        System.out.printf("Lista are marimea %d\n", myList.size());
        //metoda get() extrage obiecte din lista
        Object index0Object = myList.get(0);
        System.out.printf("Pe index %d avem obiect: %s\n", 0, index0Object);

        Long index1Object = (Long) myList.get(1);
        System.out.printf("Pe index %d avem obiect: %s\n", 1, index1Object);

        System.out.printf("obj1 == index0Object ?: %s\n", obj1 == index0Object);
        System.out.printf("Lista mea continet obj1 ?: %s\n", myList.contains(obj1));
        myList.clear();
        System.out.printf("Dupa clear avem marimea %d\n", myList.size());
        System.out.printf("Lista mea continet obj1 ?: %s\n", myList.contains(obj1));

        Long myObj = (Long) myList.get(0);
        System.out.println(myObj);
    }
}
