package md.convertit.lectia16;

import md.convertit.lectia15.interfaces.Car;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Utilizator on 02.06.2017.
 */
public class MyGarageTest {
    public static void main(String[] args) {
        //creez un obiect de tip MyCarGarage
        MyCarGarage garage = new MyCarGarage();
        garage.setName("AutoBar");
        List<Car> cars = new ArrayList<>(); //gatim lista
        //adaug 2 masini in lista
        cars.add(new Car("Volvo",2017));
        cars.add(new Car("Mercedes",2019));
        garage.setCars(cars); //setam lista de masini

        //obtinem numarul de masini din garaj
        int totalCars = garage.getCars().size();
        System.out.printf("Ïn garaj avem %d masini\n", totalCars);
        for(Car temCar: garage.getCars()){
            System.out.println(temCar);
        }
    }
}
