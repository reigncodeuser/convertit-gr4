package md.convertit.lectia16;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Utilizator on 02.06.2017.
 */
public class ListaTipizataExample {
    public static void main(String[] args) {
        //crez o lista de tip String
//        List<String> myList = new ArrayList<>();
        List<String> myList = new ArrayList<>();
        String s1 = "luni";
        String s2 = "marti";
        //adaugam obiecte in lista
        myList.add(s1);
        myList.add(s2);
        myList.add("miercuri");
        myList.add("luni");
        System.out.printf("marimea listei: %d\n", myList.size());
        System.out.printf("Contine joi ?: %s\n", myList.contains("joi"));
        System.out.printf("Contine miercuri ?: %s\n", myList.contains("miercuri"));
        int indexMiercuri = myList.indexOf("miercuri");
        System.out.printf("miercuri este pe index %s\n", indexMiercuri);
        myList.add(2, "joi");//adaugam pe index 2 obiect "joi"
        System.out.printf("marimea listei: %d\n", myList.size());
        System.out.println("**************************");
        for (int i = 0; i < myList.size(); i++) {
            String tempVarible = myList.get(i);
            System.out.printf("pe index %d avem obiect %s\n", i, tempVarible);
        }
        System.out.println("--------------------------");
        for (String tempVarible : myList) {
            System.out.printf("öbiect: %s\n", tempVarible);
        }
        System.out.println("****************************");
        Iterator<String> iterator = myList.iterator();
        while (iterator.hasNext()){
            String tempVariable = iterator.next();
            System.out.printf("in iterator: %s\n", tempVariable);
        }


    }
}
