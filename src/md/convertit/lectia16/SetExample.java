package md.convertit.lectia16;

import java.util.*;

/**
 * Created by Utilizator on 02.06.2017.
 */
public class SetExample {

    public static void main(String[] args) {
       // Set<String> mySet = new HashSet<>();
        Collection<String> mySet = new HashSet<>();
        mySet.add("luni");
        mySet.add("marti");
        mySet.add("miercuri");
        mySet.add("joi");
        mySet.add("vineri");
        System.out.printf("marime set: %d\n",mySet.size());
        //adaugam obiectul luni in set-ul nostru
        mySet.add("luni");// setul nu accepta 2 obiecte identice
        System.out.printf("marime set: %d\n",mySet.size());

        Collection<String> newList = new ArrayList<>();
        newList.addAll(mySet);
        newList.add("luni");//adaugam obiecte duble
        newList.add("marti");
        System.out.println(newList);
        //creez un treeSet cu valori initiale
        Collection<String> myTreeSet = new TreeSet<>(newList);
      //  myTreeSet.addAll(newList); - adaug prin constructor
        System.out.println("ma jos avem obiectele din treeSet");
        System.out.println(myTreeSet);
    }
}
