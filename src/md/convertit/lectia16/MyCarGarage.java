package md.convertit.lectia16;

import md.convertit.lectia15.interfaces.Car;

import java.util.List;

/**
 * Created by Utilizator on 02.06.2017.
 */
public class MyCarGarage {
    private String name;
    //definim o lista de masini
    private List<Car> cars;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
