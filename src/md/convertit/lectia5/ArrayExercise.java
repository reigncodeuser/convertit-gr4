package md.convertit.lectia5;

import javax.swing.*;

/**
 * Created by Utilizator on 26.04.2017.
 */
public class ArrayExercise {
    public static void main(String args[]) {
        //de realizat un program care inregistreaza o lista
        //de 3 studenti, introducerea datelor se va face
        //folosind diloag de input

        //declaram un array de tip String cu marimea 3
        String[]  students = new String[3];
        //populez obiectul de pe index 0
        String dialogTitle = "Adaugam nume student";
        students[0] = JOptionPane.showInputDialog(dialogTitle);
        //populam obiect de pe index 1
        students[1]= JOptionPane.showInputDialog(dialogTitle);
        //populam obiect de pe index 2
        students[2]= JOptionPane.showInputDialog(dialogTitle);

        System.out.println("0: " + students[0]);
        System.out.println("1: " + students[1]);
        System.out.println("2: " + students[2]);

    }
}
