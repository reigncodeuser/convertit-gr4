package md.convertit.lectia5;

/**
 * Created by Utilizator on 26.04.2017.
 */
public class ArraysInJava {
    public static void main(String[] args) {
        //declar o variabila de tip array de int-uri
        int[] note;
        //declar o variabila de tip array de char-uri
        // char litere[]
        //declaram si initilizam o variabila de dip array de stringuri
        String[] days = new String[7]; //am creat un tablou cu marimea 7
        //afisam valoarea de pe index 0
        System.out.println("pe index 0 avem: " + days[0]);
        // days[0] - arata valoare de pe index 0 din tabloul cu numele 'days'
        String firstDay = "Luni";
        //atribuim pe index 0 din tabloul 'days' variabila 'firstDay'
        days[0] = firstDay;
        System.out.println("pe index 0 avem: " + days[0]);
        //pe index 6 din array o sa setam valoarea "Duminica"
        days[6] = "Duminica";
        System.out.println("Pe pozitia 6 avem valoarea: " + days[6]);

        //marimea tabloului (lungimea) se afla in proprietatea "lenth" al array-ului
        //o proprietate a unui obiect se acceseaza folosind . (punct)
        // ex:  <numeObiect>.<numeProprietate> ,   days.lenth
        int lungimeTablou = days.length;
        System.out.println("Array-ul are marimea(lungimea) " + lungimeTablou);

        //initilizam array-ul litere cu 3 valori initiale predefinite
        //<tipTablou> <numeTablou> = {valoare1, valoare2, valoare3, ... etc}
        char[] litere = {'A', 'B', 'C'};
        //afisam lungimea tabloului 'litere'
        System.out.println("litere are lungimea: " + litere.length);
        //atribuim pe index 0 din 'litere' valoarea U
        litere[0] = 'U';
        //atribuim pe index 1, valoarea din index 0 a array-ului
        litere[1] = litere[0];
        // pas1 : se evalueaza expresia dupa = , si se obtine 'U'
        //pas2: se atribuie 'U' pe index 1
        //afisam la consola valoare de pe index 1
        System.out.println("pe index 1 avem : " + litere[1]);
        //verificam daca valoarea de pe index 0 este egala cu valoare de pe index 1
        boolean toCompare = (litere[0] == litere[1]); // U == U ?
        System.out.println("valori din index 0 si 1 sunt egale ?:" + toCompare);
        //verificam daca sunt egale valorile din index 0 cu cele din index 2 ?
        toCompare = (litere[0] == litere[2]);
        System.out.println("valori din index 0 si 2 sunt egale ?:" + toCompare);

        char myLetter = litere[2]; //fac referinta catre index 2 al array-ului 'litere'
        //la tipuri primitive, se ia copia valorii din variabila la care facem referinta
        System.out.println("myLetter are valoarea: " + myLetter);
        myLetter = 'S';
        //afisam la consola valoare lui myLetter apoi valoarea de pe index 2 al tabloului
        System.out.println("myLetter are valoarea: " + myLetter);
        System.out.println("pe index 2 al array-ului 'litere' avem: " + litere[2]);


    }
}
