package md.convertit.lectia5;

/**
 * Created by Utilizator on 26.04.2017.
 */
public class ArrayOfObjects {
    public static void main(String[] args) {
        //declar o variabila de tip int, notaMate
        int notaMate = 9; //variabila primitiva
        String myName = new String("John"); // obiect

        /* la tipuri primitive, operatorul de atribuire =,
         copie valoare din variabila initiala( face o copie lui 9
         si o seteaza lui notaFizica
                 */
        int notaFizica = notaMate;
        System.out.println("dupa atribuire initiala: " + notaFizica); //9
        notaFizica = 10;
        System.out.println("notaFizica:" + notaFizica); //10
        System.out.println("notaMate:" + notaMate); //9

        System.out.println("myName are valoarea: " + myName);
        //declaram o alta variabila de tip String
        String txt;
        /*
         la obiecte, operatorul de atribuire =,
         copie referinta initiala( face o copie a
       locatiei de memorie)
         */
        txt = myName; //NU se creaza un alt obiect in memorie
        System.out.println("imediat: " + (txt == myName));
        //se atribuie doar referinta
        System.out.println("txt are valoare: " + txt); //John
        txt = new String("Peter"); // pe locatia de memorie a variabilei 'txt'
        //a fost modificata valoare din 'John' in 'Peter'.
        // Deoarece  'txt' si 'myName' se refera la aceasi locatie
        //de memorie, ambele variabile au fost afectate
        System.out.println("txt are valoarea: " + txt);
        System.out.println("myName are valoarea: " + myName);
        System.out.println(myName == txt);
        //todo de clarificat teoria
    }
}
