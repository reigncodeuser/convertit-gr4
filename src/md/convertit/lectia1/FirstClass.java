package md.convertit.lectia1;


import javax.swing.*;

/**
 * Created by Utilizator on 10.04.2017.
 * Aceasta este o clasa de testare
 */
public class FirstClass {

    public static void main(String[] xxx) {
        System.out.println("Hello world");
        System.out.println("Hello Denis");
        System.out.println("Hello " + "Johny");
        System.out.println("Helloț " + 5);

        //rog utilizatorul sa introduca numele sau
        //declar o variabila de tip String
        String nume;
        //o initilizez
        nume = JOptionPane.showInputDialog("Cum te numesti ?");
        //afisam mesaj de salut:
        System.out.println("Buna ziua " + nume);
    }

}
